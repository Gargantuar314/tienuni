# My personal LaTeX package -- `tienuni.sty`

This is my personal package/preamble `tienuni.sty` for all of my LaTeX files in
university (hence `tien-uni`).

The file is a work in progress, bits and pieces collected from manuals and
<tex.stackexchange.com>. Also it is really messy and really needs a clean-up, in
terms of style as well as structure.

In any of your LaTeX files, include `\usepackage{tienuni}` with the various
(undocumented) options. Sadly, some of them are even required 😬 (*Eek!*). But
there are comments in the `tienuni.sty` file that might help.

## How to install this package

### Linux

I prefer Linux as my operating system a write Linux under. In the following, I
describe how to install my style file under Linux using *texlive*. For more
details on texlive, see
+ <https://tug.org/texlive/doc/texlive-en/texlive-en.html#x1-110002.3> on how
  the texmf directory structure looks like,
+ <https://tug.org/texlive/doc/texlive-en/texlive-en.html#x1-350003.4.6> on
  where to put custom `.sty` files.

1. **Create a new directory for your personal packages.**
   
   Just to be sure where your TEXMFHOME directory is, check the path
   ```bash
   kpsewhich -var-value=TEXMFHOME
   ```
   I will call the output from now on `TEXMFHOME`, which is usually `~/texmf`.
   To adhere to the *TeX Directory Structure*, we have to create the path
   ```bash
   TEXMFHOME/tex/latex
   ```

2. **Copy `tienuni.sty` to the current working directory.**

   Now we could copy the file `tienuni.sty` on this site to the recently created
   directory, and it should immediately work. I recommend to organise the file
   actually into a subfolder `TEXMFHOME/tex/latex/tienuni/`.

   Or using Git, do the following:
   ```bash
   mkdir TEXMFHOME/tex/latex/tienuni
   cd TEXMFHOME/tex/latex/tienuni
   git clone https:\\gitlab.com\Gargantuar314\tienuni.git \
       TEXMFHOME/tex/latex/tienuni
   ```

### Windows (Windows 10)

This is how I install my style file under Windows 10 using *MiKTeX*. For more
details, at least for MiKTeX, see
+ <https://miktex.org/faq/local-additions> on where to add custom `.sty` files,
+ <https://miktex.org/kb/texmf-roots> on how the texmf directory structure looks
  like.

1. **Create a new directory which MiKTeX should manage.**

   This can be anything and practically anywhere. I use the more conventional
   approach
   ```powershell
   C:\Users\{yourusername}\localtexmf
   ```
   where `localtexmf` is the directory name I use (anything is okay). To adhere
   to the [*TeX Directory Structure*](https://miktex.org/kb/tds), create the
   directory
   ```powershell
   .\tex\latex
   ```
   inside the just created directory. The name of this directory is obligatory.
   
   To do this all in one swoop in PowerShell:
   ```powershell
   $texmf = "$env:HOMEPATH\localtexmf\tex\latex"
   mkdir $texmf
   ```

2. **Add this directory to the "TEXMF root directories".**

   Start MiKTeX as an administrator (e.g. right-click on it to see the option).
   Go to
   ```
   Settings > Directories
   ```
   and click on the "plus" symbol to select the newly created directory.
   Finally, go to
   ```
   Tasks > Refresh file name database
   ```
   to let MiKTeX cache the new file location (this is absolutely necessary).

3. **Copy `tienuni.sty` to the current working directory.**

   Probably the easiest way would be to download the file from this site and
   move it to `.\tienuni\` or equivalently `$texmf\tienuni\` (the directory we
   just created).

   Another way would be to just use Git:
   ```powershell
   git clone https:\\gitlab.com\Gargantuar314\tienuni.git $texmf
   ```
