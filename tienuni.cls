% Either have the `.cls` file in the same directory as the `.tex` file or expose
% it globally in the `$TEXMFHOME` variable, i.e. `~/texmf/tex/latex/tienuni/`.
% Windows with MiKTeX: For security reasons, you cannot edit the MiKTeX
% installation folder, and there is no `$TEXMFHOME`. Create a `localtexmf\`
% directory somewhere and let MiKTeX know about it, e.g.
% `%HOMEPATH$\localtexmf\`. Do not forget to index the packages again in MiKTeX!
% See <https://tex.stackexchange.com/questions/69483>.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tienuni}[2024-01-27 My LaTeX document class for university]

%%% OPTIONS AND DEPENDENCIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ExplSyntaxOn
\DeclareKeys[tienuni]{
    % Language localisation: This interface currently supports exactly two
    % languages:
    % "uk": British English (default)
    % "de": German
    lang.usage          = load,
    lang.choices:nn     = {uk, de}
        {\newcommand{\@tienuni@lang}{\int_use:N\l_keys_choice_int}},

    % Document modes: Declares the type of document and is not mandatory. If
    % none is given, basic settings are loaded. Possible values are:
    % "notes": book notes and longer texts
    % "lecture": lecture notes
    % "assignment": assignments
    mode.usage          = load,
    mode.choices:nn     = {notes, lecture, assignment}
        {\newcommand{\@tienuni@mode}{\int_use:N\l_keys_choice_int}},

    % Switches: Enable or disable functionality
    nomath.ifnot        = @tienuni@math,
    nothm.ifnot         = @tienuni@thm,
    notikz.ifnot        = @tienuni@tikz,
    noref.ifnot         = @tienuni@ref,
    noidx.ifnot         = @tienuni@idx,
    lst.if              = @tienuni@lst,
    alg.if              = @tienuni@alg,
}
\ExplSyntaxOff

% Setting defaults
\SetKeys{
    lang                = uk,
}

% Meta data for `notes` mode
\DeclareKeys[notes]{
    % course: course name or main title
    course.store        = \@notes@course,
    % subtitle: topic of course or subtitle
    subtitle.store      = \@notes@subtitle,
    % author: main author (other authors possible in `assignment` mode)
    author.store        = \@notes@author,
    author.default:n    = {Tien Nguyen Thanh},
    % date: date
    date.store          = \@notes@date,
    date.default:n      = \today,
}

% Meta data for `lecture` mode. Uses all of `notes` mode.
\DeclareKeys[lecture]{
    % code: course code
    code.store          = \@lecture@code,
    % lecturer: lecturer for this course
    lecturer.store      = \@lecture@lecturer,
    % term: study term or semester
    term.store          = \@lecture@term,
}

% Meta data for `assignment` mode. Uses all of `notes` and `lecture` modes.
\DeclareKeys[assignment]{
    % authora, authorb: other authors of assignment 
    authora.store       = \@assignment@author@a,
    authorb.store       = \@assignment@author@b,
    % number: assignment number
    number.store        = \@assignment@number,
}

% Language option for `listings` package
\DeclareKeys[lst]{
    lstlang.store       = \@lst@lstlang,
    lstlang.default:n   = python,
}

% Switch for disclaimer for `lecture` mode
\DeclareKeys[dis]{
    nodis.ifnot         = @lecture@dis,
}

% Draft mode
%\DeclareOption{draft}{
%    \PassOptionsToClass{draft}{report}
%    \PassOptionsToClass{draft}{article}
%}

\DeclareUnknownKeyHandler{
    \PackageError{Key #1 is unknown to 'tienuni'.}
}

% Process options
\ProcessKeyOptions[tienuni]
\ifcase\@tienuni@mode\or
    \ProcessKeyOptions[notes]
\or
    \ProcessKeyOptions[notes]
    \ProcessKeyOptions[lecture]
    \ProcessKeyOptions[dis]
\or
    \ProcessKeyOptions[notes]
    \ProcessKeyOptions[lecture]
    \ProcessKeyOptions[assignment]
\fi
\if@tienuni@lst
    \ProcessKeyOptions[lst]
\fi
%\ProcessOptions\relax

% Load document class
\ifnum\@tienuni@mode=1
    \LoadClass{report}
\else
    \LoadClass{article}
\fi

%% Localisation: Translation of all things specific to language
%\if@tienuni@de
%    \newcommand{\@loc@disclaimer}{
%        \emph{Haftungsausschluss:} Das sind meine persönlichen Mitschriften aus
%        der Vorlesung und hängen in keiner Weise mit der Dozent*in als Person
%        oder der Universität zusammen.
%        Die Mitschriften basieren zwar auf der Vorlesung der Dozent*in, wurden
%        aber mehrfach von mir und mithilfe anderer Quellen (Personen, Bücher,
%        Internet, Tutorien) überarbeitet, sodass sie nur in ferner bis keiner
%        Weise die Vorlesung widerspiegeln.
%        Trotz großer Sorgfalt bei der Erstellung der Mitschriften sind alle
%        Angaben ohne Gewähr und Anspruch auf Vollständigkeit.
%
%        \emph{Anmerkung:} Die mit einem Stern markierten Inhalte (wie z.\,B.\
%        \emph{Bemerkung*}) und Fußnoten sind von mir hinzugefügt worden und
%        stammen nicht aus der Vorlesung.
%    }
%    \newcommand{\@loc@lecsep}{VL}
%    \newcommand{\@loc@reviewed}{Nachge-\\arbeitet}
%    \newcommand{\@loc@sheet}{Blatt}
%    \newcommand{\@loc@lecturer}{Dozent}
%    \newcommand{\@loc@notetaker}{Mitschriften von}
%    \newcommand{\@loc@update}{Stand}
%    \newcommand{\@loc@thm@theorem}{Satz}
%    \newcommand{\@loc@thm@corollary}{Korollar}
%    \newcommand{\@loc@thm@fact}{Fakt}
%    \newcommand{\@loc@thm@remark}{Bemerkung}
%    \newcommand{\@loc@thm@warning}{Warnung}
%    \newcommand{\@loc@thm@observation}{Beobachtung}
%    \newcommand{\@loc@thm@exercise}{Aufgabe}
%    \newcommand{\@loc@thm@example}{Beispiel}
%    \newcommand{\@loc@thm@solution}{Lösung}
%\else
%    \newcommand{\@loc@disclaimer}{%
%        \emph{Disclaimer:} These are my \emph{personal} notes from the lecture.
%        As I revise my notes based on books, tutorials at university and the
%        internet, my notes may not reflect the exact content of this course
%        taught by the lecturer.
%        All errors and deficiencies in these notes are certainly due to me.
%
%        \emph{Note:} Content marked with a star such as \emph{Remark*} and
%        footnotes are additions from me and did not appear during the lecture.
%    }
%    \newcommand{\@loc@lecsep}{Lect.}
%    \newcommand{\@loc@reviewed}{Reviewed}
%    \newcommand{\@loc@sheet}{Sheet}
%    \newcommand{\@loc@lecturer}{Lecturer}
%    \newcommand{\@loc@notetaker}{Notes by}
%    \newcommand{\@loc@update}{Last updated}
%    \newcommand{\@loc@thm@theorem}{Theorem}
%    \newcommand{\@loc@thm@corollary}{Corollary}
%    \newcommand{\@loc@thm@fact}{Fact}
%    \newcommand{\@loc@thm@remark}{Remark}
%    \newcommand{\@loc@thm@warning}{Warning}
%    \newcommand{\@loc@thm@observation}{Observation}
%    \newcommand{\@loc@thm@exercise}{Exercise}
%    \newcommand{\@loc@thm@example}{Example}
%    \newcommand{\@loc@thm@solution}{Solution}
%\fi
%
%% Language 
%\RequirePackage[T1]{fontenc}
%\iftienuni@de
%    \RequirePackage[ngerman]{babel}
%    \babelprovide[hyphenrules=ngerman-x-latest]{ngerman}
%\else
%    \RequirePackage[british]{babel}
%\fi
%
%% Font, typography, page layout
%\RequirePackage{lmodern}
%\RequirePackage{microtype}
%\RequirePackage{geometry}
%\RequirePackage[defaultlines=2, all]{nowidow}
%\RequirePackage{fancyhdr}
%
%% Mathematical typesetting
%\if@tienuni@math
%    \RequirePackage{mathtools}
%        \mathtoolsset{mathic}
%    \RequirePackage{amssymb}
%    \RequirePackage{bm}
%    \RequirePackage{diffcoeff}
%    \RequirePackage{siunitx}
%        \addto{\extrasngerman}{\sisetup{locale=DE}}
%        \addto{\extrasbritish}{\sisetup{locale=UK}}
%        \sisetup{
%            propagate-math-font=true,
%            text-series-to-math=true,
%        }
%    \RequirePackage{mleftright} % Reduce awkward space around `\left`/`\right`
%    \RequirePackage{aligned-overset} % Align `overset`
%\fi
%
%% Theorem environments
%\if@tienuni@thm
%    \RequirePackage{amsthm}
%\fi
%
%% Listings, pseudocode
%\if@tienuni@lst
%    \RequirePackage{listings}
%    \RequirePackage{lstautogobble} % Provides `autogobble`
%\fi
%
%% Drawings, figures, tables, algorithms
%\if@tienuni@tikz
%    \RequirePackage{tikz}
%        % `babel` also sets `handle active characters in nodes`, which is not
%        % necessary; use manual hyphenation in node labels instead.
%        % See: https://tex.stackexchange.com/questions/669671
%        \usetikzlibrary{arrows.meta, calc, fit, math, positioning}
%    \RequirePackage[compat=1.18]{pgfplots}
%    \RequirePackage{tikz-cd}
%\else
%    \RequirePackage{xcolor}
%\fi
%\RequirePackage{booktabs}
%\RequirePackage[indention=\the\parindent]{caption}
%\RequirePackage{algorithm}
%\RequirePackage[noend]{algpseudocode}
%
%% Misc
%\RequirePackage[shortlabels, inline]{enumitem}
%\RequirePackage{multicol}
%\RequirePackage[debrief, immediate]{silence}
%\RequirePackage{csquotes}
%\RequirePackage[noadjust]{marginnote}
%\if@tienuni@idx
%    \RequirePackage{makeidx}
%    \makeindex
%\fi
%\if@tienuni@notes
%    \RequirePackage{manfnt}
%\fi
%
%% References
%\iftienuni@noref\else
%    \RequirePackage{xurl}
%    \RequirePackage{hyperref}
%    \addto{\extrasbritish}{\PassOptionsToPackage{english}{cleveref}}
%    \addto{\extrasngerman}{\PassOptionsToPackage{ngerman}{cleveref}}
%    \RequirePackage[compress]{cleveref}
%    \RequirePackage[compress, english]{cleveref}
%\fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% General settings
%\geometry{a4paper, marginratio=1:1, scale=0.66666, heightrounded} % scale is 7/9
%\iftienuni@standalone\else
%    \pagestyle{fancy}
%    \iftienuni@assignment
%        \ifx\tienuni@authora\@empty
%            \fancyhead[L]{\tienuni@author}
%        \else\ifx\tienuni@authorb\@empty
%            \fancyhead[L]{\tienuni@author, \tienuni@authora}
%        \else
%            \fancyhead[L]{\tienuni@author, \tienuni@authora, \tienuni@authorb}
%        \fi\fi
%        \fancyhead[R]{\tienuni@course, \@sheet~\tienuni@number}
%    \else
%        \renewcommand*{\sectionmark}[1]{\markright{\thesection\quad #1}}
%        \fancyhead[R]{}
%        \fancyhead[L]{}
%        \fancyhead[C]{\scshape\rightmark}
%        \renewcommand{\headrulewidth}{0pt}
%    \fi
%\fi
%
%% Mathematical typesetting
%\iftienuni@nomath\else
%    % Equation counts with section or solution counter
%    \iftienuni@assignment
%        \newcounter{csolution}
%        \counterwithin{equation}{csolution}
%    \else
%        \counterwithin{equation}{section} 
%    \fi
%    \renewcommand*{\left}{\mleft}
%    \renewcommand*{\right}{\mright}
%    \difdef{f, fp}{}{
%        long-var-wrap=dv, % Do not enclose multi-letter variables (like x_i)
%        outer-Ldelim=, % Settings for evaluation at a point
%        outer-Rdelim=\bigg|,
%        sub-nudge=0mu
%    }
%    \difdef{s, sp, c, cp}{}{
%        long-var-wrap=dv, % Do not enclose multi-letter variables (like x_i)
%    }
%    % Allowing to add rules and lines similar to "tabular" in any matrix
%    % environment.  From https://texblog.net/latex-archive/maths/amsmath-matrix/
%    \renewcommand*{\env@matrix}[1][*\c@MaxMatrixCols c]{%
%        \hskip -\arraycolsep
%        \let\@ifnextchar\new@ifnextchar
%        \array{#1}%
%    }
%    \DeclareMathAlphabet{\mymathbb}{U}{bbold}{m}{n} % More blackbold letters
%    \setcounter{MaxMatrixCols}{20} % Default was 10
%\fi
%
%% Custom math notation
%\iftienuni@nomath\else
%    % Delimiters
%    \DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
%    \DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
%    \DeclarePairedDelimiter{\mset}{\lbrace}{\rbrace}
%    \DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
%    \DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
%    \DeclarePairedDelimiter{\pn}{\lparen}{\rparen}
%    \DeclarePairedDelimiter{\bk}{\lbrack}{\rbrack}
%    \DeclarePairedDelimiter{\dotp}{\langle}{\rangle}
%    % General
%    \DeclareMathOperator*{\argmin}{arg\,min}
%    \DeclareMathOperator*{\argmax}{arg\,max}
%    \iftienuni@de
%        \DeclareMathOperator{\ggt}{ggT}
%        \renewcommand{\gcd}{\ggt}
%        \DeclareMathOperator{\lcm}{kgV}
%    \else
%        \DeclareMathOperator{\lcm}{lcm}
%    \fi
%    \DeclareMathOperator{\sgn}{sgn}
%    \newcommand{\mmid}{\;\middle|\;}
%    \mathchardef\mhyph="2D
%    % Complex numbers
%    \DeclareMathOperator{\real}{Re}
%    \renewcommand{\Re}{\real}
%    \DeclareMathOperator{\imag}{Im}
%    \renewcommand{\Im}{\imag}
%    \newcommand*{\conj}[1]{\overline{#1}}
%    \newcommand{\ii}{\mathrm{i}}
%    % Logic, sets, numbers
%    \newcommand{\coloniff}{\mathrel{\vcentcolon\!\!\iff}}
%    \newcommand{\powerset}{\mathcal{P}}
%    \newcommand{\dotcup}{\mathbin{\dot{\cup}}}
%    \newcommand{\dotbigcup}{\mathop{\dot{\bigcup}}}
%    \newcommand{\symdiff}{\mathop{\triangle}}
%    \newcommand{\N}{\mathbb{N}}
%    \newcommand{\Z}{\mathbb{Z}}
%    \newcommand{\Q}{\mathbb{Q}}
%    \newcommand{\R}{\mathbb{R}}
%    \newcommand{\C}{\mathbb{C}}
%    \newcommand{\h}{\mathbb{H}}
%    \newcommand{\e}{\mathrm{e}}
%    \newcommand{\nsubset}{\not\subset}
%    \newcommand{\nequiv}{\not\equiv}
%    % Analysis
%    \newcommand{\eps}{\varepsilon}
%    \newcommand*{\close}[1]{\overline{#1}}
%    \newcommand*{\open}[1]{\mathring{#1}}
%    \newcommand{\D}{\mathrm{D}}
%    \newcommand*{\eval}[2]{\bigg\vert_{#1}^{#2}}
%    \newcommand{\Cf}{\mathcal{C}}
%    \DeclareMathOperator{\grad}{grad}
%    \DeclareMathOperator{\mdiv}{div}
%    \iftienuni@de
%        \DeclareMathOperator{\curl}{rot}
%    \else
%        \DeclareMathOperator{\curl}{curl}
%    \fi
%    % Linear algebra
%    \DeclareMathOperator{\tr}{tr}
%    \DeclareMathOperator{\rk}{rk}
%    \DeclareMathOperator{\diag}{diag}
%    \DeclareMathOperator{\mspan}{span}
%    \DeclareMathOperator{\Mat}{Mat}
%    \newcommand{\p}[1]{\begin{pmatrix}#1\end{pmatrix}}
%    \newcommand{\spm}[1]{\pn*{\begin{smallmatrix}#1\end{smallmatrix}}}
%    \newcommand{\tp}{\top}
%    \DeclareMathOperator{\GL}{\mathrm{GL}}
%    \DeclareMathOperator{\SL}{\mathrm{SL}}
%    \DeclareMathOperator{\Orth}{\mathrm{O}}
%    \DeclareMathOperator{\SO}{\mathrm{SO}}
%    \DeclareMathOperator{\U}{\mathrm{U}}
%    \DeclareMathOperator{\SU}{\mathrm{SU}}
%    \DeclareMathOperator{\Sp}{\mathrm{Sp}}
%    % Algebra
%    \DeclareMathOperator{\im}{im}
%    \DeclareMathOperator{\coker}{coker}
%    \DeclareMathOperator{\End}{End}
%    \DeclareMathOperator{\Aut}{Aut}
%    \DeclareMathOperator{\map}{map}
%    \DeclareMathOperator{\nil}{nil}
%    \DeclareMathOperator{\rad}{rad}
%    \DeclareMathOperator{\ann}{ann}
%    % Fix old arrow tip style, inspired by tikz-cd
%    \renewcommand*{\twoheadrightarrow}{
%        \mathrel{\rightarrow\mkern-15.5mu\rightarrow}
%    }
%    \newcommand*{\tilderightarrow}{
%        \mathrel{\makebox[1.2em]{\rightarrowfill}
%        \mathclap{\smash{\raisebox{0.7ex}{\hspace{-1.4em}$\scriptstyle\sim$}}}}
%    }
%    \newcommand*{\longtwoheadrightarrow}{\relbar\joinrel\twoheadrightarrow}
%    \DeclareRobustCommand{\longhookrightarrow}{\lhook\joinrel\longrightarrow}
%    \newcommand*{\longtilderightarrow}{
%        \overset{\smash{\raisebox{-0.4ex}{$\scriptstyle\sim$}}}{\longrightarrow}
%    }
%    \newcommand*{\unsim}{\smash{\raisebox{0.8ex}{$\scriptstyle\sim$}}}
%    \newcommand*{\Longtilderightarrow}{
%        \underset{\unsim}{\longrightarrow}
%    }
%    \renewcommand*{\to}[1][]{\mathchoice
%        {\ITE{#1}{\longrightarrow}{\overset{#1}{\longrightarrow}}}
%        {\rightarrow}{\rightarrow}{\rightarrow}
%    }
%    \newcommand*{\inj}[1][]{\mathchoice
%        {\ITE{#1}{\longhookrightarrow}{\overset{#1}{\longhookrightarrow}}}
%        {\hookrightarrow}{\hookrightarrow}{\hookrightarrow}
%    }
%    \newcommand*{\proj}[1][]{\mathchoice
%        {\ITE{#1}{\longtwoheadrightarrow}{\overset{#1}{\longtwoheadrightarrow}}}
%        {\twoheadrightarrow}{\twoheadrightarrow}{\twoheadrightarrow}
%    }
%    \newcommand*{\iso}[1][]{\mathchoice
%        {\ITE{#1}{\longtilderightarrow}{\overset{#1}{\Longtilderightarrow}}}
%        {\tilderightarrow}{\tilderightarrow}{\tilderightarrow}
%    }
%    \let\my@mapsto\mapsto
%    \renewcommand*{\mapsto}{\mathchoice
%        {\longmapsto}
%        {\my@mapsto}{\my@mapsto}{\my@mapsto}
%    }
%    \newcommand{\Mapsfrom}{\mathrel{\reflectbox{\ensuremath{\mapsto}}}}
%    \newcommand{\longmapsfrom}{\mathrel{\reflectbox{\ensuremath{\longmapsto}}}}
%    \newcommand*{\mapsfrom}{\mathchoice
%        {\longmapsfrom}
%        {\Mapsfrom}{\Mapsfrom}{\Mapsfrom}
%    }
%    % Group, ring, field theory, modules
%    \newcommand{\K}{\mathbb{K}}
%    \newcommand{\F}{\mathbb{F}}
%    \DeclareMathOperator{\ord}{ord}
%    \newcommand{\id}{\mathrm{id}}
%    \DeclareMathOperator{\Char}{char}
%    \newcommand{\Gal}{\mathrm{Gal}}
%    \newcommand{\Syl}{\mathrm{Syl}}
%    \DeclareMathOperator{\Ext}{Ext}
%    \DeclareMathOperator{\Tor}{Tor}
%    \DeclareMathOperator{\Quot}{Quot}
%    \DeclareMathOperator{\Spec}{Spec}
%    \DeclareMathOperator{\MaxSpec}{MaxSpec}
%    \renewcommand{\aa}{\mathfrak{a}} % Overwriting Danish a ring
%    \newcommand{\mm}{\mathfrak{m}}
%    \newcommand{\pp}{\mathfrak{p}}
%    \newcommand{\qq}{\mathfrak{q}}
%    \newcommand{\tors}{\mathrm{tors}}
%    % Number theory
%    \renewcommand{\O}{\mathcal{O}} % Overwriting Danish crossed O
%    \newcommand{\n}{\mathrm{N}}
%    \newcommand{\Cl}{\mathrm{Cl}}
%    % Category theory
%    \newcommand{\Set}{\mathsf{Set}}
%    \newcommand{\Grp}{\mathsf{Grp}}
%    \newcommand{\Ab}{\mathsf{Ab}}
%    \newcommand{\Ring}{\mathsf{Ring}}
%    \newcommand{\Field}{\mathsf{Field}}
%    \newcommand{\Vect}{\mathsf{Vect}}
%    \newcommand{\Mod}{\mathsf{Mod}}
%    \newcommand{\Alg}{\mathsf{Alg}}
%    \newcommand{\Top}{\mathsf{Top}}
%    \newcommand{\op}{\mathrm{op}}
%    \DeclareMathOperator{\ob}{ob}
%    % Topology
%    \newcommand*{\RP}{\mathbb{RP}}
%    \newcommand*{\CP}{\mathbb{CP}}
%    % Probability theory
%    \newcommand{\E}{\mathbb{E}}
%    \newcommand{\Var}{\mathrm{Var}}
%    \newcommand{\Nor}{\mathcal{N}}
%    \newcommand{\Lap}{\mathcal{L}}
%    \newcommand{\Unif}{\mathcal{U}}
%    \newcommand{\Bin}{B}
%    \newcommand{\Pois}{\mathrm{Pois}}
%    % Algorithms, graph theory
%    \newcommand{\bigo}{\mathcal{O}}
%    \DeclareMathOperator{\dist}{dist}
%\fi
%
%% Theorem environments
%\iftienuni@nothm\else
%    \iftienuni@assignment
%        % Share counter with solution (i.e. `csolution`)
%        \theoremstyle{definition}
%            \newtheorem{solution}[csolution]{\@loc@thm@solution}
%        \theoremstyle{remark}
%            \newtheorem{remark}{\@loc@thm@remark}[csolution]
%    \else
%            \newenvironment*{proof*}[1][\proofname]{
%                \begin{proof}[#1*]
%            }{
%                \end{proof}
%            }
%        % Share counter with equation
%            \newtheorem{proposition}[equation]{Proposition}
%            \newtheorem{proposition*}[equation]{Proposition*}
%            \newtheorem{lemma}[equation]{Lemma}
%            \newtheorem{lemma*}[equation]{Lemma*}
%            \newtheorem{theorem}[equation]{\@loc@thm@theorem}
%            \newtheorem{theorem*}[equation]{\@loc@thm@theorem*}
%            \newtheorem{corollary}[equation]{\@loc@thm@corollary}
%            \newtheorem{corollary*}[equation]{\@loc@thm@corollary*}
%            \newtheorem{fact}[equation]{\@loc@thm@fact}
%            \newtheorem{fact*}[equation]{\@loc@thm@fact*}
%        \theoremstyle{definition}
%            \newtheorem{definition}[equation]{Definition}
%            \newtheorem{definition*}[equation]{Definition*}
%            \newtheorem{notation}[equation]{Notation}
%            \newtheorem{notation*}[equation]{Notation*}
%            \newtheorem{axiom}[equation]{Axiom}
%            \newtheorem{remark}[equation]{\@loc@thm@remark}
%            \newtheorem{remark*}[equation]{\@loc@thm@remark*}
%            \newtheorem{warning}[equation]{\@loc@thm@warning}
%            \newtheorem{warning*}[equation]{\@loc@thm@warning*}
%            \newtheorem{observation}[equation]{\@loc@thm@observation}
%            \newtheorem{observation*}[equation]{\@loc@thm@observation*}
%            \newtheorem{exercise}[equation]{\@loc@thm@exercise}
%            \newtheorem{exercise*}[equation]{\@loc@thm@exercise*}
%        \theoremstyle{remark}
%            \newtheorem{problem}[equation]{Problem}
%            \newtheorem{example}[equation]{\@loc@thm@example}
%            \newtheorem{example*}[equation]{\@loc@thm@example*}
%            \newtheorem*{solution}{\@loc@thm@solution}
%        \iftienuni@notes
%            % https://tex.stackexchange.com/questions/705255
%            \newlength{\dangerwidth} % width of \dbend
%            \settowidth{\dangerwidth}{\dbend}
%            \addtolength{\dangerwidth}{1em} % add any extra space between \dbend and text
%
%            \renewenvironment{warning}[1][]{
%                \par\addvspace{\topsep} % default \topsep before theorem environment
%                \AddToHookNext{para/after}{ % add hook only once after very first paragraph
%                    \ifnum\prevgraf=1 % if paragraph only consists of one line
%                        \nopagebreak\hbox{} % add empty line without page break
%                    \fi
%                }
%                \hangindent=\dangerwidth \hangafter=-2 % specifying indentation of first two lines
%                \refstepcounter{equation} % I mostly use equation counter, feel free to use any other one
%                \everypar\expandafter{%
%                    {\setbox0=\lastbox}% % remove indentation box, avoid \noindent for vertical mode
%                    \makebox[0pt]{\hspace*{-\hangindent}\dbend\hfill}% % zero width \dbend, shifted to left boarder
%                    \textbf{\@loc@thm@warning\ \theequation}% % theorem name and number
%                    \if\relax\detokenize{#1}\relax\else % optional argument, check if empty
%                        \space(#1)% % if so, type optional argument in parenthesis
%                    \fi
%                    \textbf{.}\hspace*{5pt plus 1pt minus 1pt}% % add default rubber after theorem header
%                    \everypar{}% reset \everypar
%                }
%            }{
%                \par\addvspace{\topsep}% % default \topsep after environment
%            }
%            \renewcommand{\qedsymbol}{\manimpossiblecube}
%        \fi
%    \fi
%\fi
%
%% Listings
%% Note: use `\verb` in math mode since it's more robust (no ttfamily error)
%\iftienuni@nolst\else
%    \lstloadlanguages{bash, [11]c++, python}
%    \lstset{
%        breaklines,
%        floatplacement=htb,
%        language=\tienuni@lstlang,
%        basicstyle=\ttfamily,
%        numbers=left,
%        numberstyle=\tiny\color{gray},
%        numbersep=5pt,
%        captionpos=b,
%        xleftmargin=15pt,
%        xrightmargin=3pt,
%        breakatwhitespace,
%        frame=single,
%        autogobble,
%        extendedchars=true,
%        literate={ä}{{\"a}}1 {ö}{{\"o}}1 {ü}{{\"u}}1 {ß}{{\ss}}1 {Ä}{{\"A}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1,
%    }
%    \lst@AddToHook{DisplayStyle}{\small}
%    \newcommand*{\lstinputshort}[2][]{%
%        \lstinputlisting[float, caption=\lstinline|#2|], #1]{#2}%
%    }
%\fi
%
%% Pseudocode
%\iftienuni@noalg\else
%    \iftienuni@de
%        \floatname{algorithm}{Algorithmus}
%    \else
%        \floatname{algorithm}{Algorithm}
%    \fi
%    %`\algnewcommand{\Goto}{\item[\algorithmicgoto]}` as an example for `\Require` style commands
%    \algnewcommand{\algorithmicinput}{\makebox[0pt][l]{\textbf{input:}}\phantom{\textbf{output:}}}
%    \algnewcommand{\Input}{\item[\algorithmicinput]}
%    \algnewcommand{\algorithmicoutput}{\textbf{output:}}
%    \algnewcommand{\Output}{\item[\algorithmicoutput]}
%    %\algrenewcommand{\algorithmicrequire}{\makebox[0pt][l]{\textbf{input:}}\phantom{\textbf{output:}}}
%    %\algrenewcommand{\algorithmicensure}{\textbf{output:}}
%    \algnewcommand{\algorithmicset}{\textbf{set}}
%    \algnewcommand{\Set}{\algorithmicset\ }
%    \algnewcommand{\algorithmicto}{\textbf{to}}
%    \algnewcommand{\To}{\algorithmicto\ }
%    \algnewcommand{\algorithmicdownto}{\textbf{downto}}
%    \algnewcommand{\Downto}{\algorithmicdownto\ }
%    \algnewcommand{\algorithmicout}{\textbf{output}}
%    \algnewcommand{\Out}{\algorithmicout\ }
%    \algnewcommand{\algorithmicgoto}{\textbf{goto}}
%    \algnewcommand{\Goto}{\algorithmicgoto\ }
%    \algnewcommand{\algorithmicstop}{\textbf{stop}}
%    \algnewcommand{\Stop}{\algorithmicstop\ }
%    % See: https://tex.stackexchange.com/questions/669665/
%    \newcommand{\algparenthesize}[1]{%
%        \if\relax\detokenize{#1}\relax\else(#1)\fi%
%    }
%    \algdef{SExnE}[PROCEDURE]{Procedure}{EndProcedure}[2]
%        {\algorithmicprocedure\ \textproc{#1}\algparenthesize{#2}}
%    \algdef{SExnE}[FUNCTION]{Function}{EndFunction}[2]
%        {\algorithmicfunction\ \textproc{#1}\algparenthesize{#2}}
%    \algrenewcommand{\Call}[2]{\textproc{#1}\algparenthesize{#2}}
%    % New breakable environment for algorithm: By default, algorithm is a float
%    % and thus no page break is possible. The following mimics the design of the
%    % algorithm environment without using floats. See:
%    % https://tex.stackexchange.com/questions/33866
%    \iffalse
%    \renewenvironment{algorithm}{%
%        \begin{center}
%            \refstepcounter{algorithm}%
%            \hrule height.8pt depth0pt \kern2pt% \@fs@pre for \@fs@ruled from float package
%            \renewcommand{\caption}[2][\relax]{% Make new \caption
%                {\raggedright\textbf{\fname@algorithm~\thealgorithm} ##2\par}%
%                %The following adds the environment to list of algorithms (loa) in the toc
%                %\ifx\relax##1\relax
%                %    \addcontentsline{loa}{algorithm}{\protect\numberline{\thealgorithm}##2}%
%                %\else
%                %    \addcontentsline{loa}{algorithm}{\protect\numberline{\thealgorithm}##1}%
%                %\fi
%                \kern2pt\hrule\kern2pt%
%            }%
%    }{%
%            \kern2pt\hrule\relax% \@fs@post for \@fs@ruled
%        \end{center}
%    }
%    \fi
%\fi
%
%%Drawings, figures and tables
%\newcommand*{\colsep}{\hskip 3\tabcolsep} %Custom length for separating columns a bit more; `\tabcolsep` stores only half of the actually column separation, since it is added on the left and right columns
%
%%Float settings
%\g@addto@macro{\@floatboxreset}{\centering} %Add `\centering`
%%Add `htb`, `lstlisting` is defined in its section respectively
%\renewcommand*{\fps@figure}{htb}
%\renewcommand*{\fps@table}{htb}
%%Redefine counters
%\renewcommand*{\c@table}{\c@figure}
%\renewcommand*{\thetable}{\thefigure}
%\renewcommand*{\ftype@figure}{\ftype@table}
%\counterwithin{figure}{section}
%\counterwithin{table}{section}
%%The displayed `section` counter is replaced with the `solution` counter, but with every `solution` call, the `section` mechanism is not triggered, meaning the figure numbers do not reset
%\iftienuni@assignment
%    \@addtoreset{figure}{solution}
%\fi
%
%% Optional nolst
%% lstlisting needs special treatment since its counter is defined at `\AtBeginDocument`
%\iftienuni@nolst\else
%    \AtBeginDocument{
%        \renewcommand*{\c@lstlisting}{\c@figure}
%        \renewcommand*{\thelstlisting}{\thefigure}
%        \renewcommand*{\ftype@figure}{\ftype@lstlisting}
%        \counterwithin{lstlisting}{section}
%    }
%\fi
%% Optional noalg
%\iftienuni@noalg\else
%    \renewcommand*{\fps@algorithm}{htb}
%    \renewcommand*{\c@algorithm}{\c@figure}
%    \renewcommand*{\thealgorithm}{\thefigure}
%    \renewcommand*{\ftype@figure}{\ftype@algorithm}
%    \counterwithin{algorithm}{section}
%\fi
%
%% References
%\iftienuni@noref\else
%    \hypersetup{
%        bookmarksnumbered,
%        hidelinks,
%        pdfauthor={\tienuni@author},
%        pdftitle={\tienuni@course}
%    }
%    \crefalias{theorem*}{theorem}
%    \crefalias{proposition*}{theorem}
%    \crefalias{lemma*}{lemma}
%    \crefalias{corollary*}{corollary}
%    \crefalias{fact*}{fact}
%    \crefalias{definition*}{definition}
%    \crefalias{notation*}{notation}
%    \crefalias{remark*}{remark}
%    \crefalias{warning*}{warning}
%    \crefalias{observation*}{observation}
%    \crefalias{exercise*}{exercise}
%    \crefalias{example*}{example}
%    \crefname{equation}{}{}
%    \crefname{enumi}{}{}
%    \iftienuni@de
%        \crefname{theorem}{Satz}{Sätze}
%        \crefname{fact}{Fakt}{Fakten}
%        \crefname{observation}{Beobachtung}{Beobachtungen}
%        \crefname{notation}{Notation}{Notationen}
%        % Abuse capitalised commands for German genitive case
%        %\Crefname{theorem}{Satzes}{Sätzen}
%        %\Crefname{lemma}{Lemmas}{Lemmata}
%    \else
%        \crefname{fact}{fact}{facts}
%    \fi
%\fi
%
%% Title
%\iftienuni@lecture
%    \title{\vspace*{-2\baselineskip}\bfseries
%        \tienuni@code{}~-- \tienuni@course\\[.2\baselineskip]
%        \large\tienuni@subtitle
%    }
%    \author{
%        {\small \@lecturer:}\\\textsc{\tienuni@lecturer}
%        \and
%        {\small \@notetaker:}\\\textsc{\tienuni@author}
%    }
%    \date{
%        \tienuni@term\\
%        {\small \@update: \tienuni@date}
%    }
%\fi
%\iftienuni@notes
%    \title{\vspace*{-2\baselineskip}\bfseries
%        \tienuni@course
%    }
%    \author{\textsc{\tienuni@author}}
%    \date{\tienuni@date}
%\fi
%\iftienuni@assignment
%    \title{\vspace*{-2\baselineskip}
%        {\large\tienuni@course{}~-- \tienuni@lecturer{}~-- \tienuni@term}\\[.2\baselineskip]
%        \@sheet~\tienuni@number
%    }
%    \ifx\tienuni@authora\@empty
%        \author{\tienuni@author}
%    \else\ifx\tienuni@authorb\@empty
%        \author{\tienuni@author \and \tienuni@authora}
%    \else
%        \author{\tienuni@author \and \tienuni@authora \and \tienuni@authorb}
%    \fi\fi
%    \date{\tienuni@date}
%\fi
%
%% Disclaimer for lecture mode
%\iftienuni@lecture\iftienuni@nodisclaimer\else
%    \g@addto@macro{\maketitle}{
%        \vspace*{-\baselineskip}
%        \begin{quotation}
%            \small\@disclaimer
%        \end{quotation}
%    }
%\fi\fi
%
%% Lecture separator for lecture mode
%\iftienuni@lecture
%    \newcounter{clecturesep}
%    % Alias to `\hrulefill`, but raised by `height`
%    \iffalse
%    \def\@hrulefill{\leavevmode\leaders\hrule height 0.65ex depth \dimexpr0.4pt-0.65ex\hfill\kern\z@} 
%    \newcommand*{\lecturesep}[1]{%
%        \stepcounter{clecturesep}%
%        \vspace{\baselineskip}\noindent\@hrulefill%
%        \fbox{Ende der Vorlesung~\theclecturesep{} am {#1}}%
%        \@hrulefill\vspace{\baselineskip}%
%    }
%    \fi
%    \newcommand*{\lsep}[1]{%
%        \stepcounter{clecturesep}%
%        \marginnote{\normalsize\normalfont
%            \@lecsep~\theclecturesep{}\\
%            {#1}
%        }%
%    }
%\fi
%
%% Misc
%\iftienuni@notikz\else
%    \tikzset{
%        handle active characters in code,
%        >=Stealth,
%        dot/.pic={\filldraw circle[radius=1.5pt];},
%        pics/xtick/.style args={#1}{
%            code={\draw (0,-0.1) node[below] {#1} -- (0,0.1);}
%        },
%        pics/ytick/.style args={#1}{
%            code={\draw (-0.1,0) node[left] {#1} -- (0.1,0);}
%        },
%        pics/xtickr/.style args={#1}{
%            code={\draw (0,-0.1) -- (0,0.1) node[above] {#1};}
%        },
%        pics/ytickr/.style args={#1}{
%            code={\draw (-0.1,0) -- (0.1,0) node[right] {#1};}
%        },
%        vertex/.style={
%            circle, draw, fill=black, inner sep=0pt, outer sep=0pt, minimum size=1.5mm
%        }
%    }
%\fi
%\iftienuni@lecture
%    \renewcommand*{\l@subsection}{\@dottedtocline{2}{1.5em}{2.7em}}
%    \renewcommand*{\l@subsubsection}{\@dottedtocline{3}{3.8em}{3.6em}}
%    \newcommand*{\reviewed}{
%        \@ifundefined{alreadyreviewed}{
%                \marginpar{\small\bfseries\@reviewed}
%            \newcommand{\alreadyreviewed}{1}
%        }{
%            \PackageError{tienuni}{Cannot have two 'reviewed' tags.}{}
%        }
%    }
%    % Remove title of `thebibliography`
%    \let\my@thebibliography\thebibliography
%    \let\endmy@thebibliography\endthebibliography
%    \renewenvironment*{thebibliography}[1]{%
%        \begingroup
%        \renewcommand*{\section}[2]{}
%        \renewcommand*{\markboth}[2]{}
%        \begin{my@thebibliography}{#1}
%    }{
%        \end{my@thebibliography}
%        \endgroup
%    }
%    \AtBeginDocument{\maketitle\tableofcontents}
%    \iftienuni@noidx\else
%        \AtEndDocument{\printindex}
%    \fi
%\fi
%\iftienuni@assignment\else
%    \setlist[enumerate, 1]{label=\textnormal{(\roman*)}, widest=viii}
%    \setlist[enumerate, 2]{label=\textnormal{(\alph*)}, widest=m}
%\fi
%\iftienuni@noidx
%    \newcommand*{\defemph}[1]{\textbf{#1}}
%\else
%    \newcommand*{\defemph}[1]{\textbf{#1}\index{#1}}
%\fi
%    
%% Items with custom text and possibility to give labels, must use with `\ref`
%% See https://tex.stackexchange.com/questions/405645
%\newcommand*{\refitem}[1][]{
%    \item[#1] \protected@edef\@currentlabel{\textnormal{#1}}%
%}
%\WarningFilter{latex}{Font shape `T1/lmr/m/scit' undefined}
%% Fix bug with microtype, wrong protrusion
%\renewcommand*{\csq@setmarker@close}{}
%\renewcommand*{\csq@setmarker@open}{}
%% `highlight` environment
%\iftienuni@nothm\else
%    \newenvironment{highlight}[2][]{
%        \par\addvspace{.5\topsep}\addvspace{-\parskip}
%        \noindent\textit{#2}%
%        \ITE{#1}{}{\space(#1)}%
%        \textit{.}\hspace*{5pt plus 1pt minus 1pt}\ignorespaces
%    }{
%        \par\addvspace{.5\topsep}
%    }
%    \newenvironment{highlight*}[2][]{
%        \par\addvspace{.5\topsep}\addvspace{-\parskip}
%        \noindent\textit{#2*}%
%        \ITE{#1}{}{\space(#1)}%
%        \textit{.}\hspace*{5pt plus 1pt minus 1pt}\ignorespaces
%    }{
%        \par\addvspace{.5\topsep}
%    }
%\fi
%% No header for TOC
%\g@addto@macro{\tableofcontents}{\markboth{\contentsname}{\contentsname}}
%% Non-breaking and breaking explicit hyphens using `babel`
%\newcommand*{\nbhyph}{\babelhyphen{nobreak}\ignorespaces}
%\newcommand*{\bhyph}{\babelhyphen{hard}\ignorespaces}
%% Emulate `\addsec` from KOMA
%\newcommand*{\addsec}[2][]{
%    \setcounter{secnumdepth}{0}
%    \IfValueTF{#1}{\section{#2}}{\section[#1]{#2\markboth{#1}{#1}}}
%    \setcounter{secnumdepth}{3}
%}
%\fi
%\endinput
